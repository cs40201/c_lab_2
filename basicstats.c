# include <stdio.h>
# include <stdlib.h>
# include <math.h>
#include "readfile.h"
/**Author: Hanqiao Lu
Student ID: A20324072
Date: 17th, April, 2016
This program is to calculate basic statistics for a list of numbers in text file.
# of numbers, mean, median, deviation and # of unused allocated dynamic memory addresses.
User need to provide the path of the text file in the command window.*/



/**Here we define a structure to 
hold all information about an array*/
struct array_info {
    float * start;
    int size;
    int capacity;
} array_info;

/**This function takes in a file path, 
read in the file to a dynamic allocated array.
Automatically expand array whenever needed.
Return: pointer to the first element in the array.*/
struct array_info read_in_arr(char path[]){
/**Read in data from txt file
and store them as an array of floats.
Return the pointer to the first element in array*/
    FILE* fp = open_file(path);
    struct array_info info;
    if (fp == NULL){
        fprintf(stderr, "The path to the input txt file is not correct.\n\n");
        exit(1);
    };

    float *arr;
    int capacity = 20;
    arr = malloc(capacity * sizeof(float));
    float *p = arr;
    int counter = 0;
    float number = 0;
    int flag = fscanf(fp, "%f\n", &number);

    /*Read all data line by line*/
    while(flag > 0){
    *p = number;
    counter += 1;
    p += 1;
    /*Reallocate a space if # of numbers exceeds 
    the maximum length of memory.*/
    if (counter > capacity){
        capacity *= 2;
        float *new_arr = malloc(capacity * sizeof(float));
	int i = 0;
	for(i = 0; i < counter; i++){
           new_arr[i] = arr[i];	
	}
	free(arr);
	arr = new_arr;
	p = arr + counter;
    }
    flag = fscanf(fp, "%f\n", &number);
    };

    close_file(fp);
    info.size = counter;
    info.start = arr;
    info.capacity = capacity;
    return info;
    };

/**Calculate the mean value of array.
@params: data: the array to be calculated.
	size: the length of array
Return: mean value of the list of numbers.*/
double mean(float data[], int size){
	double result = 0;
	int i = 0;
	for (i = 0; i<size; i++){
		result += data[i];
	}
	return result / (double)size;
};

/**Calculate the median value of the array.
@params: data: the array to be calculated.
	size: the length of array
Return: median value of the list of numbers.*/
double median(float data[], int size){
    /*Sort the array for finding median*/
    int comp(const void *a, const void *b) {
        return ( *(float*)a - *(float*)b );
    };

    qsort(data, size, sizeof(float), comp);
    if (size % 2 == 0){
	return (data[(int)(size/2) - 1] + data[(int)(size/2)])/2;
	}
    else{
	return data[(int)(size/2)];
	}
};

/**Calculate the standard deviation of the array of numbers.
@params: data: the array to be calculated.
	size: the length of array
Return: standard deviation of the list of numbers.*/
double sdv(float data[], int size){
	int i;
	double sum_sq;
	double m = mean(data, size);
	for(i = 0; i<size; i++){
		sum_sq += (data[i] - m) * (data[i] - m);
	}
	return sqrt(sum_sq / size);
};

/**The main driver. 
Print out the mean, median and deviation of the list of numbers.
*/
int main (int argc, char *argv[]){
    struct array_info info = read_in_arr(argv[1]);
    if (info.size == 0){
        return -1;
    }
    printf("Results:\n-------");
    printf("\nNum values:\t%d\n      mean:\t%.3lf\n    median:\t%.3lf\n    stddev:\t%.3lf\nUnused array capacity: %d\n", info.size, mean(info.start, info.size), median(info.start, info.size), sdv(info.start, info.size), info.capacity - info.size);
    free(info.start);
};



