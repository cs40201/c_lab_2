#ifndef READFILE_H_INCLUDED
#define READFILE_H_INCLUDED
#include <stdio.h>
/*Header file of readfile library
 handling database I/O.
 Author: Hanqiao Lu
 Date: 1th, April, 2016*/
FILE * open_file(char file_name[]);

int close_file(FILE *fp);

int read_int(int *p);

int read_float(float *p);

int read_string(char s[]);

#endif // READFILE_H_INCLUDED
