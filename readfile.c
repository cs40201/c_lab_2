#include <stdio.h>
/*readfile library for handling database I/O.
Author: Hanqiao Lu
Date, 2th, April, 2016
*/

/*The function to open file containing data.*/
FILE* open_file(char file_name[]){
    FILE *fp;
    fp = fopen(file_name, "r+");
    if (fp <= 0) return -1;
    return fp;
};

/*Close the file*/
int close_file(FILE *fp) {
    return fclose(fp);
};

/*Read in an integer from std input*/
int read_int(int *p){
    if (scanf("%d", p) <= 0){
    return -1;
    }else {
    return 0;
    }
};

/*Read in a float number from std input*/
int read_float(float *p){
    if(scanf("%f", p) == 1){
        return 0;
    }else {
        return -1;
    };
};

/*Read in a string from std input.*/
int read_string(char s[]){
    if(scanf("%s", s) > 0){
        return 0;
    }else {
        return -1;
    }
};











